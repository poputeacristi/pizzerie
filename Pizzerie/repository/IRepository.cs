﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzerie.util;

namespace Pizzerie.repository
{
    public interface IRepository<T> : IObservable<T> where T : IDObject
    {
        bool addObject(T obj);
        bool removeObject(IComparable id);
        T find(IComparable id);
        List<T> getAll();

        void close();
    }
}
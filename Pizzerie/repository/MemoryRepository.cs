﻿//using Pizzerie.domain;
using Pizzerie.util.TAD;
using Pizzerie.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerie.repository
{
    public class MemoryRepository<T> : IRepository<T> where T : IDObject
    {
        protected IDictionar<IComparable, T> data = new DictionarArray<IComparable, T>();
        private List<IObserver<T>> observers = new List<IObserver<T>>();

        public bool addObject(T obj)
        {
            if (find(obj.ID) == null)
            {
                data.adauga(obj.ID, obj);
                this.Notify(obj);
                return true;
            }
            return false;
        }

        public bool removeObject(IComparable id)
        {
            T obj = data.cauta(id);
            bool ret = data.elimina(id);
            this.Notify(obj);
            
            return ret;
        }

        public T find(IComparable id)
        {
            return data.cauta(id);
        }

        public List<T> getAll()
        {
            return data.getAll();
        }

        virtual public void close()
        {

        }

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber<T>(observers, observer);
        }

        private void Notify(T next)
        {
            foreach(IObserver<T> ob in observers)
            {
                ob.OnNext(next);
            }
        }
    }
}

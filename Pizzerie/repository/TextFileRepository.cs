﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pizzerie.util;
using System.Globalization;

namespace Pizzerie.repository
{
    public delegate String IdObject2String<T>(T obj); 
    public delegate T String2IdObject<T>(String s); 
    public class TextFileRepository<T> : MemoryRepository<T> where T:IDObject
    {
        private String _fileName;
        private IdObject2String<T> obj;
        private String2IdObject<T> s;
        public TextFileRepository(String fileName, IdObject2String<T> obj, String2IdObject<T> s)
            : base()
        {
            this._fileName = fileName;
            this.obj = obj;
            this.s = s;
            this.loadFromFile();
        }

        private bool saveToFile()
        {
            try
            {
                System.IO.StreamWriter file = new System.IO.StreamWriter(_fileName);
                List<T> lista = base.getAll();
                foreach(T elem in lista)
                {
                    file.WriteLine(obj(elem)+'\n');
                }
                file.Close();
           }
            catch (System.IO.FileNotFoundException)
            {
                throw new Exception("Nu se poate deschide fisierul " + _fileName);
            }
            catch (System.IO.IOException)
            {
                throw new Exception("Nu se poate citi din fisierul " + _fileName);
            }
            return false;
        }

        private void loadFromFile()
        {
            System.IO.StreamReader file = new System.IO.StreamReader(_fileName);

            while (file.EndOfStream==false)
            {
                String line = file.ReadLine();
                file.ReadLine();

                if (line != "") 
                { 
                    T t = s(line);
                    data.adauga(t.ID,t);
                }
            }
            file.Close();
        }

        public override void close()
        {
            this.saveToFile();
        }
    }
}

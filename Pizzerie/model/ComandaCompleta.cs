﻿using Pizzerie.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerie.model
{
    public class ComandaCompleta:IDObject
    {
        //Atribute
        private int _id;
        private Comanda _com;
        private string _numeChelner;
        private string _stare;

        #region proprietati
        public int Id
        {
            get { return _id; }
        }

        public Comanda Comanda
        {
            get { return _com; }
        }

        public String NumeChelner
        {
            get { return _numeChelner; }
            set { _numeChelner = value; }
        }

        public String Stare
        {
            get { return _stare; }
            set { _stare = value; }
        }
        #endregion
        public ComandaCompleta(int Id,Comanda Com,String NumeChelner,String Stare)
            :base(Id)
        {
            _id = Id;
            _com = Com;
            _numeChelner = NumeChelner;
            _stare = Stare;
        }

        public override String ToString()
        {
            return "Id:" + Id.ToString() + " Chelner:" + NumeChelner + " " + Comanda.ToString();// + " " + Stare;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerie.model
{
    [Serializable]
    public class Comanda
    {
        //Atribute:
        private String _tipPizza;
        private int _nrMasa;
        private int _bucati;
        private String _ora;


        public String TipPizza
        {
            get { return _tipPizza; }
            set { _tipPizza = value; }
        }

        public String Ora
        {
            get { return _ora; }
            set { _ora = value; }
        }

        public int NrMasa
        {
            get { return _nrMasa; }
            set { _nrMasa = value; }
        }

        public int Bucati
        {
            get { return _bucati; }
            set { _bucati = value; }
        }
        //Consturctor

        public Comanda(String TipPizza, int NrMasa,int Bucati,String Ora)
        {
            _tipPizza = TipPizza;
            _nrMasa = NrMasa;
            _bucati = Bucati;
            _ora = Ora;
        }

        public override String ToString()
        {
            return "Tip Pizza:"+TipPizza+" NrMasa:"+NrMasa+" Bucati:"+Bucati+" Ora:"+Ora;
        }
    }
}

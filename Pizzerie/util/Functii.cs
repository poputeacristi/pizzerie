﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
using Pizzerie.model;

namespace Pizzerie.util
{
    public static class Util
    {
        public static List<string> loadFromFile(String fileName)
        {
            List<string> nume = new List<string>();
            System.IO.StreamReader file = new System.IO.StreamReader(fileName);
            char[] delimiterChars = { '\t' };

            while (file.EndOfStream == false)
            {
                String line = file.ReadLine();
                nume.Add(line);

            }
            file.Close();
            return nume;
        }

        public static int genereazaNumar(int nr){
            // functia genereaza un numar intreg intre 0 si nr 
             
            Random rnd = new Random();
            return rnd.Next(nr);
        }

        #region delegates

        public static String ComandaCompleta2String(ComandaCompleta c)
        {
            StringBuilder buff = new StringBuilder("");

            buff.Append(c.ID + ";");
            buff.Append(c.Comanda.TipPizza + ";");
            buff.Append(c.Comanda.NrMasa + ";");
            buff.Append(c.Comanda.Bucati + ";");
            buff.Append(c.Comanda.Ora + ";");
            buff.Append(c.NumeChelner + ";");
            buff.Append(c.Stare + ";");

            return buff.ToString();
        }
        public static ComandaCompleta String2ComandaCompleta(String s)
        {
            char[] delimiterChars = { ';', '\t' };
            string[] words = s.Split(delimiterChars);

            int id = Int32.Parse(words[0]);
            string tipPizza = words[1];
            int NrMasa = Int32.Parse(words[2]);
            int Bucati = Int32.Parse(words[3]);
            string Ora = words[4];
            string NumeChelner = words[5];
            string Stare = words[6];

            Comanda c = new Comanda(tipPizza, NrMasa, Bucati, Ora);
            ComandaCompleta com = new ComandaCompleta(id, c, NumeChelner, Stare);


            return com;
        }


        #endregion

    }

    public static class Util<T>
    {
        public static void loadListbox(System.Windows.Forms.ListBox listBox, List<T> list)
        {
            foreach (T elem in list)
                listBox.Items.Add(elem);
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzerie.util
{
    class ParolaChelner
    {
        private string combo;
        private string temp = "";
        private int cont = 0;

        private PictureBox picBox;
        private string parola;
        private string filenameD;
        private string filenameChelner;
        public ParolaChelner(PictureBox picBox, string parola,string filenameD,string filenameChelner)
        {
            this.picBox = picBox;
            this.combo = parola;
            this.filenameD = filenameD;
            this.filenameChelner = filenameChelner;
        }
        public void update(Keys pressedkey)
        {
            temp = pressedkey.ToString();

            if ("D" == temp)
                picBox.Load(filenameD);
            else
            {
                if (temp[0] == combo[cont])
                    cont++;
                else
                    cont = 0;
                if (cont == combo.Length)
                {
                    try
                    {
                        picBox.Load(filenameChelner);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    } cont = 0;
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Pizzerie.util.TAD
{
    [Serializable]
    public class DictionarArray<K, T> : IDictionar<K, T>, IEnumerable<T> where K:IComparable
    {
        private List<Pair> data;

        public DictionarArray()
        {
            data = new List<Pair>();
        }

        public bool adauga(K cheie, T elem)
        {
            /**
             * Trebuie sa adugam astfel incat sa pastram 
             * vectorul sortat
             **/
            Pair val = new Pair(cheie, elem);

            try
            {
                if (cauta(cheie) != null)
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }

            for (int i = 0; i <= data.Count - 1; i++)
            {
                K cheie1 = data[i].Cheie;

                if (cheie1.CompareTo(cheie) > 0)
                {
                    try
                    {
                        data.Insert(i, val);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return true;
                }

            }


            try
            {
                data.Add(val);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;

        }

        public T cauta(K cheie)
        {
            /*
             Cautare binara
             */
            //try
            //{
                int init = 0, sf = data.Count, mid;
                if (data.Count == 0 || data.Count==-1)
                    return default(T);
                do
                {
                    mid = (sf + init) / 2;
                    K cheie1 = data[mid].Cheie;
                    if (cheie1.CompareTo(cheie) == 0)
                        return data[mid].Valoare;
                    if (cheie1.CompareTo(cheie) > 0)
                        sf = mid;
                    else
                        init = mid + 1;

                } while (init < sf);

                //}
            //catch
            //{
              //  throw new Exception("Dynamic Array error, the search function is not working proprley");
            //}
            return default(T);
        }

        public bool elimina(K cheie)
        {


            bool ok = false;
            for (int i = 0; i <= data.Count - 1; i++)
            {

                if (data[i].Cheie.Equals(cheie))
                {
                    try
                    {
                        data.RemoveAt(i);
                        ok = true;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
            return ok;
        }

        public bool setVal(K cheie, T elem)
        {
            try
            {


                if (elimina(cheie) == false)
                    return false;

                return adauga(cheie, elem);
            }
            catch
            {
                throw new Exception("DictionarArray error, the setVal is not working proprely");
            }
        }

        public List<T> getAll()
        {
            List<T> lista=new List<T>();

            for (int i = 0; i <= data.Count - 1; i++)
                lista.Add(data[i].Valoare);

            return lista;
        }

        #region InnerClasses
        [Serializable]
        class Pair  //clasa pereche care contine cheia si valoarea
        {
            private K cheie;
            private T valoare;

            public K Cheie
            {
                get { return cheie; }
                set { cheie = value; }
            }

            public T Valoare
            {
                get { return valoare; }
                set { valoare = value; }
            }
            public Pair(K pcheie, T pval)
            {
                cheie = pcheie;
                valoare = pval;
            }
        }

        //am folosit IEnumarator in TEst_DictionarArray in functia de test 2 la for each
        public IEnumerator<T> GetEnumerator()
        {

            for (int i = 1; i <= data.Count - 1; i++)////data.Size
                yield return data[i].Valoare;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion
    }
}

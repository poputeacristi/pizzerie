﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Pizzerie.util.TAD
{
    public interface IDictionar<K, T> where K : IComparable
    {
        bool adauga(K cheie, T elem);
        T cauta(K cheie);
        bool elimina(K cheie);
        bool setVal(K cheie, T elem);
        List<T> getAll();
    }
}

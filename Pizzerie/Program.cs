﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

using Pizzerie.controller;
using Pizzerie.repository;
using Pizzerie.util;
using Pizzerie.view;
using Pizzerie.model;


namespace Pizzerie
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //creez controlerul si il dau viewului
            Controller ctrl = new Controller(new TextFileRepository<ComandaCompleta>(@"..\..\data\Comenzi.txt", Util.ComandaCompleta2String, Util.String2ComandaCompleta));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new view.Pizzerie(ctrl));
            
        }


    }


}

﻿namespace Pizzerie.view
{
    partial class Bucatar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listComenziAsteptare = new System.Windows.Forms.ListBox();
            this.btnFinalizat = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listComenziAsteptare
            // 
            this.listComenziAsteptare.FormattingEnabled = true;
            this.listComenziAsteptare.HorizontalScrollbar = true;
            this.listComenziAsteptare.Location = new System.Drawing.Point(12, 38);
            this.listComenziAsteptare.Name = "listComenziAsteptare";
            this.listComenziAsteptare.Size = new System.Drawing.Size(369, 147);
            this.listComenziAsteptare.TabIndex = 0;
            // 
            // btnFinalizat
            // 
            this.btnFinalizat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinalizat.Location = new System.Drawing.Point(152, 191);
            this.btnFinalizat.Name = "btnFinalizat";
            this.btnFinalizat.Size = new System.Drawing.Size(101, 30);
            this.btnFinalizat.TabIndex = 1;
            this.btnFinalizat.Text = "Finalizat";
            this.btnFinalizat.UseVisualStyleBackColor = true;
            this.btnFinalizat.Click += new System.EventHandler(this.btnFinalizat_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 24);
            this.label1.TabIndex = 7;
            this.label1.Text = "Comenzi In Asteptare:";
            // 
            // Bucatar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 228);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFinalizat);
            this.Controls.Add(this.listComenziAsteptare);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Bucatar";
            this.Text = "Bucatar";
            this.Load += new System.EventHandler(this.Bucatar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listComenziAsteptare;
        private System.Windows.Forms.Button btnFinalizat;
        private System.Windows.Forms.Label label1;
    }
}
﻿namespace Pizzerie.view
{
    partial class Chelner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listComenziFinalizate = new System.Windows.Forms.ListBox();
            this.btnPlatit = new System.Windows.Forms.Button();
            this.dateOra = new System.Windows.Forms.DateTimePicker();
            this.nrmBuc = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbTipPizza = new System.Windows.Forms.ComboBox();
            this.nrmMasa = new System.Windows.Forms.NumericUpDown();
            this.btnADD = new System.Windows.Forms.Button();
            this.lblNume = new System.Windows.Forms.Label();
            this.picBox = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nrmBuc)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nrmMasa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).BeginInit();
            this.SuspendLayout();
            // 
            // listComenziFinalizate
            // 
            this.listComenziFinalizate.FormattingEnabled = true;
            this.listComenziFinalizate.HorizontalScrollbar = true;
            this.listComenziFinalizate.Location = new System.Drawing.Point(12, 29);
            this.listComenziFinalizate.Name = "listComenziFinalizate";
            this.listComenziFinalizate.Size = new System.Drawing.Size(253, 147);
            this.listComenziFinalizate.TabIndex = 0;
            this.listComenziFinalizate.SelectedIndexChanged += new System.EventHandler(this.listComenziFinalizate_SelectedIndexChanged);
            // 
            // btnPlatit
            // 
            this.btnPlatit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlatit.Location = new System.Drawing.Point(12, 182);
            this.btnPlatit.Name = "btnPlatit";
            this.btnPlatit.Size = new System.Drawing.Size(101, 30);
            this.btnPlatit.TabIndex = 1;
            this.btnPlatit.Text = "Platit";
            this.btnPlatit.UseVisualStyleBackColor = true;
            this.btnPlatit.Click += new System.EventHandler(this.btnPlatit_Click);
            // 
            // dateOra
            // 
            this.dateOra.CustomFormat = "HH:mm tt";
            this.dateOra.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateOra.Location = new System.Drawing.Point(177, 89);
            this.dateOra.Name = "dateOra";
            this.dateOra.ShowUpDown = true;
            this.dateOra.Size = new System.Drawing.Size(73, 20);
            this.dateOra.TabIndex = 2;
            // 
            // nrmBuc
            // 
            this.nrmBuc.Location = new System.Drawing.Point(177, 129);
            this.nrmBuc.Name = "nrmBuc";
            this.nrmBuc.Size = new System.Drawing.Size(73, 20);
            this.nrmBuc.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cmbTipPizza);
            this.panel1.Controls.Add(this.nrmMasa);
            this.panel1.Controls.Add(this.nrmBuc);
            this.panel1.Controls.Add(this.dateOra);
            this.panel1.Location = new System.Drawing.Point(12, 218);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(253, 159);
            this.panel1.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Numar bucati:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Ora comenzi:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "Numar Masa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Tip Pizza:";
            // 
            // cmbTipPizza
            // 
            this.cmbTipPizza.FormattingEnabled = true;
            this.cmbTipPizza.Location = new System.Drawing.Point(108, 11);
            this.cmbTipPizza.Name = "cmbTipPizza";
            this.cmbTipPizza.Size = new System.Drawing.Size(142, 21);
            this.cmbTipPizza.TabIndex = 5;
            this.cmbTipPizza.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbTipPizza_KeyPress);
            // 
            // nrmMasa
            // 
            this.nrmMasa.Location = new System.Drawing.Point(177, 49);
            this.nrmMasa.Name = "nrmMasa";
            this.nrmMasa.Size = new System.Drawing.Size(73, 20);
            this.nrmMasa.TabIndex = 4;
            // 
            // btnADD
            // 
            this.btnADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnADD.Location = new System.Drawing.Point(164, 182);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(101, 30);
            this.btnADD.TabIndex = 6;
            this.btnADD.Text = "Adauga Comanda";
            this.btnADD.UseVisualStyleBackColor = true;
            this.btnADD.Click += new System.EventHandler(this.btnADD_Click);
            // 
            // lblNume
            // 
            this.lblNume.AutoSize = true;
            this.lblNume.BackColor = System.Drawing.Color.Transparent;
            this.lblNume.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNume.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNume.Location = new System.Drawing.Point(283, 348);
            this.lblNume.Name = "lblNume";
            this.lblNume.Size = new System.Drawing.Size(112, 22);
            this.lblNume.TabIndex = 7;
            this.lblNume.Text = "Nume Chelner";
            // 
            // picBox
            // 
            this.picBox.Location = new System.Drawing.Point(283, 23);
            this.picBox.Name = "picBox";
            this.picBox.Size = new System.Drawing.Size(219, 347);
            this.picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBox.TabIndex = 8;
            this.picBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(170, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Lista comenzi Finalizate:";
            // 
            // Chelner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 385);
            this.Controls.Add(this.lblNume);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.picBox);
            this.Controls.Add(this.btnADD);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnPlatit);
            this.Controls.Add(this.listComenziFinalizate);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Chelner";
            this.Text = "Chelner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Chelner_FormClosing);
            this.Load += new System.EventHandler(this.Chelner_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nrmBuc)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nrmMasa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listComenziFinalizate;
        private System.Windows.Forms.Button btnPlatit;
        private System.Windows.Forms.DateTimePicker dateOra;
        private System.Windows.Forms.NumericUpDown nrmBuc;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbTipPizza;
        private System.Windows.Forms.Button btnADD;
        private System.Windows.Forms.Label lblNume;
        private System.Windows.Forms.NumericUpDown nrmMasa;
        private System.Windows.Forms.PictureBox picBox;
        private System.Windows.Forms.Label label5;
    }
}
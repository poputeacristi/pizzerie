﻿using Pizzerie.util;
//using Pizzerie.domain;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzerie.view
{
    public partial class Bucatar : Form, IObserver<Object>
    {
        private controller.Controller ctrl;

        public Bucatar()
        {
            InitializeComponent();
        }

        public Bucatar(controller.Controller ctrl)
        {
            InitializeComponent();
            this.ctrl = ctrl;
            this.ctrl.Subscribe(this);
        }

        private void Bucatar_Load(object sender, EventArgs e)
        {
            reload();
        }

        private void btnFinalizat_Click(object sender, EventArgs e)
        {
            if (listComenziAsteptare.SelectedIndex == -1)
                MessageBox.Show("Nu ai selectat nicio comanda");
            else
            {
                listComenziAsteptare.SelectedItem.ToString();
                string[] words = listComenziAsteptare.SelectedItem.ToString().Split(' ',':');

                ctrl.updateComanda(Convert.ToInt32(words[1]), "Finalizat");
            }
        }

        #region observer
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Object value)
        {
            reload();
        }
        #endregion
        private void reload()
        {
            listComenziAsteptare.Items.Clear();

            Util<string>.loadListbox(listComenziAsteptare, ctrl.getLista("Astept"));
        }
    }
}

﻿using Pizzerie.controller;
//using Pizzerie.domain;
using Pizzerie.repository;
using Pizzerie.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzerie.view
{

    public partial class Pizzerie : Form ,IObserver<Object>
    {
        private Controller ctrl;
        public Pizzerie()
        {
            InitializeComponent();
            
        }

        public Pizzerie(Controller ctrl)
        {
            InitializeComponent();
            this.ctrl = ctrl;
            this.ctrl.Subscribe(this);
        }

        private void Pizzerie_Load(object sender, EventArgs e)
        {
            List<string> nume = Util.loadFromFile(@"..\..\data\Chelneri.txt");

            foreach(string nr in nume)
                cmbChelneri.Items.Add(nr);

            reload();
        }


        private void cmbChelneri_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = (char)Keys.None;
        }

        private void cmbChelneri_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnChelener.Enabled = true;
        }

        private void btnChelener_Click(object sender, EventArgs e)
        {
            Chelner ch = new Chelner(cmbChelneri.Text,ctrl);
            ch.Show();
        }



        private void Pizzerie_FormClosing(object sender, FormClosingEventArgs e)
        {
            ctrl.close();
        }

        private void btnBucatar_Click(object sender, EventArgs e)
        {
            Bucatar buc = new Bucatar(ctrl);
            buc.Show();
        }


        #region Observer
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Object value)
        {
            //nu fac nimic cu valoarea care s-a modificat
            reload();
        }
        #endregion

        private void reload()
        {
            listComenziToate.Items.Clear();
            listComenziFinalizate.Items.Clear();
            listComenziPlatite.Items.Clear();

            Util<string>.loadListbox(listComenziToate, ctrl.getLista());
            Util<string>.loadListbox(listComenziFinalizate, ctrl.getLista("Finalizat"));
            Util<string>.loadListbox(listComenziPlatite, ctrl.getLista("Platit"));
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            ctrl.deleteAll();
            //reload();
        }
    }
}

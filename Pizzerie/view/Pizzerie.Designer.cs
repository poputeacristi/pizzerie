﻿namespace Pizzerie.view
{
    partial class Pizzerie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChelener = new System.Windows.Forms.Button();
            this.btnBucatar = new System.Windows.Forms.Button();
            this.listComenziToate = new System.Windows.Forms.ListBox();
            this.listComenziFinalizate = new System.Windows.Forms.ListBox();
            this.listComenziPlatite = new System.Windows.Forms.ListBox();
            this.cmbChelneri = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnChelener
            // 
            this.btnChelener.Enabled = false;
            this.btnChelener.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChelener.Location = new System.Drawing.Point(292, 181);
            this.btnChelener.Name = "btnChelener";
            this.btnChelener.Size = new System.Drawing.Size(113, 39);
            this.btnChelener.TabIndex = 0;
            this.btnChelener.Text = "Chelner";
            this.btnChelener.UseVisualStyleBackColor = true;
            this.btnChelener.Click += new System.EventHandler(this.btnChelener_Click);
            // 
            // btnBucatar
            // 
            this.btnBucatar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBucatar.Location = new System.Drawing.Point(292, 237);
            this.btnBucatar.Name = "btnBucatar";
            this.btnBucatar.Size = new System.Drawing.Size(113, 39);
            this.btnBucatar.TabIndex = 1;
            this.btnBucatar.Text = "Bucatar";
            this.btnBucatar.UseVisualStyleBackColor = true;
            this.btnBucatar.Click += new System.EventHandler(this.btnBucatar_Click);
            // 
            // listComenziToate
            // 
            this.listComenziToate.FormattingEnabled = true;
            this.listComenziToate.HorizontalScrollbar = true;
            this.listComenziToate.Location = new System.Drawing.Point(292, 40);
            this.listComenziToate.Name = "listComenziToate";
            this.listComenziToate.Size = new System.Drawing.Size(241, 95);
            this.listComenziToate.TabIndex = 2;
            // 
            // listComenziFinalizate
            // 
            this.listComenziFinalizate.FormattingEnabled = true;
            this.listComenziFinalizate.HorizontalScrollbar = true;
            this.listComenziFinalizate.Location = new System.Drawing.Point(21, 40);
            this.listComenziFinalizate.Name = "listComenziFinalizate";
            this.listComenziFinalizate.Size = new System.Drawing.Size(241, 95);
            this.listComenziFinalizate.TabIndex = 3;
            // 
            // listComenziPlatite
            // 
            this.listComenziPlatite.FormattingEnabled = true;
            this.listComenziPlatite.HorizontalScrollbar = true;
            this.listComenziPlatite.Location = new System.Drawing.Point(21, 181);
            this.listComenziPlatite.Name = "listComenziPlatite";
            this.listComenziPlatite.Size = new System.Drawing.Size(241, 95);
            this.listComenziPlatite.TabIndex = 4;
            // 
            // cmbChelneri
            // 
            this.cmbChelneri.FormattingEnabled = true;
            this.cmbChelneri.Location = new System.Drawing.Point(412, 192);
            this.cmbChelneri.Name = "cmbChelneri";
            this.cmbChelneri.Size = new System.Drawing.Size(121, 21);
            this.cmbChelneri.TabIndex = 5;
            this.cmbChelneri.SelectedIndexChanged += new System.EventHandler(this.cmbChelneri_SelectedIndexChanged);
            this.cmbChelneri.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbChelneri_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Comenzi Finalizate:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(288, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 24);
            this.label2.TabIndex = 6;
            this.label2.Text = "Toate Comenzile:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 24);
            this.label3.TabIndex = 6;
            this.label3.Text = "Comenzi Platite:";
            // 
            // btnDelete
            // 
            this.btnDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Location = new System.Drawing.Point(412, 237);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(113, 39);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "Delete All";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // Pizzerie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(553, 298);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbChelneri);
            this.Controls.Add(this.listComenziPlatite);
            this.Controls.Add(this.listComenziFinalizate);
            this.Controls.Add(this.listComenziToate);
            this.Controls.Add(this.btnBucatar);
            this.Controls.Add(this.btnChelener);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Pizzerie";
            this.Text = "Pizzerie";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Pizzerie_FormClosing);
            this.Load += new System.EventHandler(this.Pizzerie_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnChelener;
        private System.Windows.Forms.Button btnBucatar;
        private System.Windows.Forms.ListBox listComenziToate;
        private System.Windows.Forms.ListBox listComenziFinalizate;
        private System.Windows.Forms.ListBox listComenziPlatite;
        public System.Windows.Forms.ComboBox cmbChelneri;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDelete;
    }
}


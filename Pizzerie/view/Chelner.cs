﻿using Pizzerie.controller;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pizzerie.util;

namespace Pizzerie.view
{
    public partial class Chelner : Form,IObserver<Object>
    {
        private string numeChelner;
        private Controller ctrl;
        ParolaChelner p;
        IDisposable unsubscribe;
        public Chelner()
        {
            InitializeComponent();
        }

        public Chelner(string numeChelner,Controller ctrl)
        {
            InitializeComponent();
            this.numeChelner = numeChelner;
            this.ctrl = ctrl;
            unsubscribe =this.ctrl.Subscribe(this);

            p = new ParolaChelner(picBox, "PASS", @"..\..\data\poze\Default.jpg", @"..\..\data\poze\" + numeChelner + ".jpg");
        }

        private void Chelner_Load(object sender, EventArgs e)
        {
            List<string> nume = Util.loadFromFile(@"..\..\data\Meniu.txt");
            foreach (string nr in nume)
                cmbTipPizza.Items.Add(nr);

                picBox.Load(@"..\..\data\poze\Default.jpg");

                try
                {
                    picBox.Load(@"..\..\data\poze\" + numeChelner + ".jpg");
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
                lblNume.Text = numeChelner;

                //decomenteaz-o cand predai aplicatia
                picBox.Load(@"..\..\data\poze\Default.jpg");

                reload();
        }
 
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            p.update(keyData);
            // Call the base class
            return base.ProcessCmdKey(ref msg, keyData);
        }
        private void cmbTipPizza_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar=='d')
                picBox.Load(@"C:\Users\Chris\Documents\Pizzerie\Pizzerie\data\poze\Default.jpg");
            e.KeyChar = (char)Keys.None;
        }

        private void btnADD_Click(object sender, EventArgs e)
        {
                try
                {
                    string cmb = cmbTipPizza.Text;
                    ctrl.addComanda(cmbTipPizza.Text,
                                   Convert.ToInt32(nrmMasa.Value),
                                   Convert.ToInt32(nrmBuc.Value),
                                   dateOra.Text,
                                   numeChelner.ToString(),
                                   "Astept");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
       }

        private void btnPlatit_Click(object sender, EventArgs e)
        {
            if (listComenziFinalizate.SelectedIndex == -1)
                MessageBox.Show("Nu ai selectat nicio comanda");
            else
            {
                listComenziFinalizate.SelectedItem.ToString();
                string[] words = listComenziFinalizate.SelectedItem.ToString().Split(' ',':');

                try
                {
                    ctrl.updateComanda(Convert.ToInt32(words[1]), "Platit");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                }
        }

        #region observer
        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(Object value)
        {
            reload();
        }
        #endregion
        private void reload()
        {
            listComenziFinalizate.Items.Clear();

            Util<string>.loadListbox(listComenziFinalizate, ctrl.getLista("Finalizat",numeChelner));
        }

        private void listComenziFinalizate_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbTipPizza.Text = "";
            int crt;
            
            string[] atribute = listComenziFinalizate.SelectedItem.ToString().Split(' ',':');

            //ca sa salvez in combobox de unde trebuie adica tipul 
            //pizza incepe de la 4 si dupa aia fac for in functie decate
            //cuvinte sunt in tippizza
            for (crt = 4; crt < atribute.Count() && atribute[crt] != "NrMasa"; crt++)
                cmbTipPizza.Text += atribute[crt]+" ";
            crt++;
            nrmMasa.Value = Convert.ToInt32(atribute[crt]);
            crt += 2;
            nrmBuc.Value = Convert.ToInt32(atribute[crt]);
            dateOra.Text=atribute[crt+2]+":"+atribute[crt+3]+" "+atribute[crt+4];
        }

        private void Chelner_FormClosing(object sender, FormClosingEventArgs e)
        {
            unsubscribe.Dispose();
        }

    }
}

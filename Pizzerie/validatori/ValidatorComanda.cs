﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pizzerie.model;

namespace Pizzerie.validator
{
    public class ValidatorComanda : IValidator<Comanda>
    {
        private StringBuilder Msg = new StringBuilder("");

        public bool validate(Comanda c)
        {
            
            if (c.TipPizza == null || c.TipPizza=="")
                Msg.Append("Nu ai ales Tipul de Pizza;");
            if (c.Bucati == 0)
                Msg.Append("Nu ai ales numarul de buc pentru comanda;");
            if (c.NrMasa == 0)
                Msg.Append("Nu ai ales numarul mesei;");
            if (c.Ora == "" || c.Ora == null)
                Msg.Append("Nu ai ales ora comenzi;");
            
            if (Msg.ToString() == "")
                return true;
            else
                throw new Exception("Comanda nu a fost bine initializata deoarece: " + Msg.ToString());
        }
    }
}

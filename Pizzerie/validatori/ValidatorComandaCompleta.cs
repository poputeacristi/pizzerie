﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Pizzerie.model;

namespace Pizzerie.validator
{
    public class ValidatorComandaCompleta : IValidator<ComandaCompleta>
    {
        private StringBuilder Msg = new StringBuilder("");

        public bool validate(ComandaCompleta c)
        {
            if (c.NumeChelner == "" || c.Stare == null)
                Msg.Append("Nu ai specificat numele chelnerului pentru comanda;");
            if (c.Id >9999 || c.Id<0)
                Msg.Append("Id-ul ales nu apartine intervalului [0,9999]");
            if (c.Stare != "Finalizat" && c.Stare != "Astept" && c.Stare != "Platit" && c.Stare != "" && c.Stare == null)
                Msg.Append("Starea nu a fost bine setata");
            try
            {
                ValidatorComanda vali = new ValidatorComanda();
                vali.validate(c.Comanda);
            }
            catch (Exception ex)
            {
                Msg.Append(ex.Message);
            }

            if (Msg.ToString() == "")
                return true;
            else
                throw new Exception("Comanda Completa nu a fost bine initializata deoarece: " + Msg.ToString());
        }

    }
}

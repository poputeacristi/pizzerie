﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerie.validator
{
    public interface IValidator<T>
    {
        bool validate(T elem);
    }
}

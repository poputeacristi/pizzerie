﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzerie.repository;
using Pizzerie.util;
using Pizzerie.model;
using System.Runtime.InteropServices;
using Pizzerie.validator;

namespace Pizzerie.controller
{
    public class Controller
    {
        private IRepository<ComandaCompleta> _repo;
        private int id;//id-ul comenzi
        public Controller(IRepository<ComandaCompleta> Repo)
        {
            _repo = Repo;
            id = 1;
        }

        #region operatii crud
        //TO DO VALIDATORI SI EXCEPTII
        public bool addComanda(string TipPizza, int nrMasa, int Bucati, string Ora,
            string numeChelner, string Stare)
        {

            //daca exista deja alta comanda cu acest id atunci genereaza alt id
            //id-ul poate sa reapara daca contruiesc un alt controller
            //care foliseste acelasii repostiory de aceea mai verific
            while (_repo.find(id) != null)
                id++;

            Comanda c = new Comanda(TipPizza, nrMasa, Bucati, Ora);

            ComandaCompleta cmp = new ComandaCompleta(id, c, numeChelner, Stare);

            ValidatorComandaCompleta vali = new ValidatorComandaCompleta();
            try
            {
                vali.validate(cmp);
            }
            catch(Exception ex)
            {
                throw ex;
            } 
            

            return _repo.addObject(cmp);
        }

        public bool updateComanda(int id,[Optional]string Stare, [Optional]string TipPizza, 
            [Optional]int nrMasa,[Optional]int Bucati,  [Optional] string Ora, 
            [Optional]string numeChelner)
        {
            //returneaz fals daca nu exista aceasta comanda

            ComandaCompleta cmp = _repo.find(id);
            if (cmp == null)
                return false;

            if(Stare!=null)
                cmp.Stare = Stare;
            if(TipPizza!=null)
                cmp.Comanda.TipPizza = TipPizza;
            if(nrMasa!=0)
                cmp.Comanda.NrMasa = nrMasa;
            if (Bucati != 0)
                cmp.Comanda.Bucati = Bucati;
            if (Ora != null)
                cmp.Comanda.Ora = Ora;
            if (numeChelner != null)
                cmp.NumeChelner = numeChelner;

            ValidatorComandaCompleta vali = new ValidatorComandaCompleta();

            try
            {
                vali.validate(cmp);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            _repo.removeObject(id);
            return _repo.addObject(cmp);
        }
        public string findComanda(int id)
        {
            ComandaCompleta c = _repo.find(id);
            if (c == null)
                return null;
            return c.ToString();
        }
        public bool removeObject(IComparable id)
        {
            return _repo.removeObject(id);
        }
        public ComandaCompleta find(IComparable id)
        {
            return _repo.find(id);
        }
        public List<ComandaCompleta> getAll()
        {
            return _repo.getAll();
        }

        public void close()
        {
            _repo.close();
        }
        #endregion

        public List<string> getLista([Optional]string Stare,[Optional] string numeChelner)
        {
            //returneaza lista care contine comenzile completa dupa o anumita stare
            //in format string
            List<string> list = new List<string>();

            if (Stare != null)
            {
                foreach (ComandaCompleta elem in _repo.getAll())
                    if (elem.Stare == Stare && (numeChelner == elem.NumeChelner || numeChelner == null))
                    
                        if(numeChelner!=elem.NumeChelner)
                            list.Add(elem.ToString());
                        else
                        {   //aici elmin numele chelnerului pt ca ma aflu pe formul lui
                            //din cauza elseului sunt stiu ca sunt in formul acestuia
                            string[] atribute=elem.ToString().Split(' ');
                            StringBuilder afisare=new StringBuilder("");
                            for (int i = 0; i < atribute.Count(); i++)
                            {
                                if (i != 2 && i != 1) { 
                                    afisare.Append(atribute[i]);
                                    afisare.Append(" ");}
                            }
                            list.Add(afisare.ToString());
                        }

                    
            }
            else
                foreach (ComandaCompleta elem in _repo.getAll())
                        list.Add(elem.ToString());
                
            return list;
        }

        public IDisposable Subscribe(IObserver<ComandaCompleta> ob)
        {
            return _repo.Subscribe(ob);
        }

        public void deleteAll()
        {
            List<ComandaCompleta> all = this.getAll();

            foreach (ComandaCompleta elem in all)
            {
                this.removeObject(elem.Id);
            }
        }
    }
}

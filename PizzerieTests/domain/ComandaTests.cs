﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pizzerie.model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace Pizzerie.util.Tests
{
    [TestClass()]
    public class ComandaTests
    {
        [TestMethod()]
        public void ComandaTest()
        {
            Comanda c = new Comanda("Pizza", 23, 2, "12am");

            Assert.AreEqual(c.TipPizza, "Pizza");
            Assert.AreEqual(c.NrMasa, 23);
            Assert.AreEqual(c.Bucati, 2);
            Assert.AreEqual(c.Ora, "12am");

            c.TipPizza = "Diavola";
            c.NrMasa = 11;
            c.Bucati = 5;
            c.Ora = "11am";

            Assert.AreNotEqual(c.TipPizza, "Pizza");
            Assert.AreNotEqual(c.NrMasa, 23);
            Assert.AreNotEqual(c.Bucati, 2);
            Assert.AreNotEqual(c.Ora, "12am");

            Assert.AreEqual(c.TipPizza, "Diavola");
            Assert.AreEqual(c.NrMasa, 11);
            Assert.AreEqual(c.Bucati, 5);
            Assert.AreEqual(c.Ora, "11am");
        }

        [TestMethod()]
        public void ToStringTest()
        {
            Comanda c = new Comanda("Pizza", 23, 2, "12am");
            Assert.AreNotEqual(c.ToString(), "1 Quatro 22 2 11am");
            Assert.AreNotEqual(c.ToString(), "Pizza 23 2 12am");
            Assert.AreEqual(c.ToString(), "Tip Pizza:Pizza NrMasa:23 Bucati:2 Ora:12am");
            Assert.IsTrue(true);
        }
    }
}
